<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Scopes\UsersToDosScope;

class Todo extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable: removed , 'user_id'
     *
     * @var array
     */
    protected $fillable = [
        'title', 'complete', 'description'
    ];

    /**
     *
     * Attributes that should be casted to either a date, array or boolean
     *
     * @var array
     */
    protected $casts = [
        'complete' => 'boolean',
        'user_id' => 'int'
    ];

    /**
     * The "booted" method of the model.
     *
     * @return void
     */
    protected static function booted()
    {
        static::addGlobalScope(new UsersToDosScope);
    }
}
