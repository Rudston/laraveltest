<?php

namespace App\Http\Controllers;

use App\Models\Todo;
use App\Http\Requests\CreateToDoRequest;
use App\Http\Requests\UpdateToDoRequest;
use Illuminate\Http\Request;

class TodoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
//    public function index()
//    {
//        return Todo::where('user_id', auth('sanctum')->user()->id)->get();
//    }

    /**
     * @param Request $request
     * @return array
     */
    public function index(Request $request)
    {
        $query = ToDo::query();

        $perPage = 15;
        $page = $request->input('page', 1);
        $total = $query->count();

        $result = $query->offset(($page - 1) * $perPage)->limit($perPage)->get();
        return [
            'data' => $result,
            'total' => $total,
            'page' => $page,
            'last_page' => ceil($total / $perPage)
        ];
    }

    /**
     * @param CreateToDoRequest $request
     * @return mixed
     */
    public function store(CreateToDoRequest $request)
    {
        $toDo = Todo::create(
            $request->validated()
        );

        $toDo->user_id = auth('sanctum')->user()->id;

        return $toDo->save();
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\Todo $todo
     * @return \Illuminate\Http\Response
     */
    public function show(Todo $todo)
    {
        return $todo;
    }

    /**
     * @param UpdateToDoRequest $request
     * @param Todo $todo
     * @return bool
     */
    public function update(UpdateToDoRequest $request, Todo $todo)
    {
        return $todo->update($request->validated());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\Todo $todo
     * @return \Illuminate\Http\Response
     */
    public function destroy(Todo $todo)
    {
        return response()->json([
            'success' => !!$todo->delete()
        ]);
    }
}
